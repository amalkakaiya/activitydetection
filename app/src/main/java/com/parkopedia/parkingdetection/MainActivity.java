package com.parkopedia.parkingdetection;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class MainActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static GoogleApiClient mGoogleApiClient;
    private PendingIntent mActivityRecognitionPendingIntent;
    private PendingIntent mLocationRequestPendingIntent;
    private static final String TAG = "ActivityRecognition - Main";
    private int frequency = 0;
    private Intent locationIntent;
    private Intent mServiceIntent;
    private MainActivity mActivity;
    private EditText mFreqText;
    private TextView statusText;
    private Boolean status;
    private LocationRequest mLocationRequest;

    @Override
    public void onConnected(Bundle bundle) {
        mActivityRecognitionPendingIntent = PendingIntent.getService(mActivity, 0, mServiceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mGoogleApiClient, frequency, mActivityRecognitionPendingIntent).setResultCallback(
            new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    if (status.isSuccess()) {
                        Log.i(TAG, "Successfully registered updates");
                        Toast.makeText(getApplicationContext(), "GoogleApiClientConnection Started", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i(TAG, "Failed to register updates");
                    }
                }
            }
        );

        createLocationRequest();
        mLocationRequestPendingIntent = PendingIntent.getService(mActivity, 1, locationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, mLocationRequestPendingIntent);
    }

    @Override
    public void onConnectionSuspended(int i) {

        Toast.makeText(this, "GoogleApiClientConnection Suspended", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("status", status);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null){
            status = false;
        } else {
            status = savedInstanceState.getBoolean("status");
        }
        Button mBtnStartService = (Button) findViewById(R.id.startService);
        mBtnStartService.setOnClickListener(startServiceListener);
        Button mBtnStopService = (Button) findViewById(R.id.stopService);
        mBtnStopService.setOnClickListener(stopServiceListener);
        Button mBtnParkIn = (Button) findViewById(R.id.parkIn);
        mBtnParkIn.setOnClickListener(parkListener);
        Button mBtnParkOut = (Button) findViewById(R.id.parkOut);
        mBtnParkOut.setOnClickListener(parkListener);
        mFreqText = (EditText) findViewById(R.id.freqText);
        Button mUpdateFreq = (Button) findViewById(R.id.updateFreq);
        mUpdateFreq.setOnClickListener(updateFrequencyListener);
        statusText = (TextView) findViewById(R.id.labelStatus);
        statusText.setText(status.toString());
        Button btnWriteLog = (Button) findViewById(R.id.writeLog);
        btnWriteLog.setOnClickListener(writeToLogListener);

        mServiceIntent = new Intent(getApplicationContext(), ActivityRecognitionIntentService.class);
        mServiceIntent.setAction("ActivityRecognitionIntentService");

        locationIntent = new Intent(getApplicationContext(), ActivityRecognitionIntentService.class);
        locationIntent.setClass(getApplicationContext(), ActivityRecognitionIntentService.class);

        updateStatusText();

    }

    private View.OnClickListener writeToLogListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LogWriterDialog logWriterDialog = new LogWriterDialog();
            logWriterDialog.show(getFragmentManager(), "Write To Log");
        }
    };

    private View.OnClickListener updateFrequencyListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                Integer freq = Integer.parseInt(mFreqText.getText().toString());
                if (freq >= 0){
                    frequency = freq;
                    if (mGoogleApiClient != null && mGoogleApiClient.isConnected()){
                        if (mActivityRecognitionPendingIntent != null){
                            mActivityRecognitionPendingIntent = PendingIntent.getService(mActivity, frequency, mServiceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        }
                    }
                    Toast.makeText(mActivity, "Updated frequency to: "+frequency+"ms", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e){
                Toast.makeText(mActivity, "Unable to update Frequency", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }
    };

    private View.OnClickListener startServiceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!status){
                mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addApi(ActivityRecognition.API)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(mActivity)
                    .addOnConnectionFailedListener(mActivity)
                    .build();
                mGoogleApiClient.connect();
                Toast.makeText(mActivity, "Appending results to file"+ Environment.getExternalStorageDirectory().getPath()+"/activityDetection.log", Toast.LENGTH_LONG).show();
                status = true;
                updateStatusText();
            } else {
                Toast.makeText(mActivity, "Service is already started", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void updateStatusText(){
        statusText.setText( status ? "Running..." : "Stopped" );
        if (status) {
            statusText.setTextColor(getResources().getColor(R.color.green));
        } else {
            statusText.setTextColor(getResources().getColor(R.color.red));
        }
    }

    private View.OnClickListener stopServiceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (status) {
                if (mGoogleApiClient != null) {
                    ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(mGoogleApiClient, mActivityRecognitionPendingIntent);
                    if (mGoogleApiClient.isConnected()) {
                        mGoogleApiClient.disconnect();
                    }
                    mActivityRecognitionPendingIntent.cancel();
                    mLocationRequestPendingIntent.cancel();
                    status = false;
                    updateStatusText();
                }
            } else {
                Toast.makeText(mActivity, "Service is not started", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener parkListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (status){
                Intent park = new Intent(getApplicationContext(), ActivityRecognitionIntentService.class);
                park.setClass(getApplicationContext(), ActivityRecognitionIntentService.class);
                park.setAction("ActivityRecognitionIntentService");
                if (v.getId() == R.id.parkIn) {
                    park.putExtra("Park", "PARK_IN");
                } else {
                    park.putExtra("Park", "PARK_OUT");
                }
                startService(park);
                Toast.makeText(mActivity, "Park Registered", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mActivity, "Error, try starting service", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, LogActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
}

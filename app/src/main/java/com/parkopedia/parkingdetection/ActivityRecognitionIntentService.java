package com.parkopedia.parkingdetection;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Environment;
import android.util.Log;
import com.google.android.gms.location.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ActivityRecognitionIntentService extends IntentService {

    private static final String TAG ="ActivityRecognition";
    private static Location currentLocation;
    private static long locationTimeStamp;
    private static double mLat = 0;
    private static double mLng = 0;

    public ActivityRecognitionIntentService() {
        super("ActivityRecognitionIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String loc = getLocationString();
            if (intent.hasExtra("Park")){
                String parkType = intent.getStringExtra("Park");
                StringBuilder sb = new StringBuilder()
                    .append(System.currentTimeMillis())
                    .append(",")
                    .append(parkType)
                    .append(",")
                    .append("null,")
                    .append(loc);
                appendLog(sb.toString());
                Log.d(TAG, parkType+" Received");
            } else if (intent.hasExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED)){
                currentLocation = intent.getParcelableExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED);
                locationTimeStamp = System.currentTimeMillis();
                mLng = currentLocation.getLongitude();
                mLat = currentLocation.getLatitude();
                Log.d(TAG, "Location object Received");
                Log.d(TAG, "lat:"+currentLocation.getLatitude()+" lng:"+currentLocation.getLongitude());
                Log.d(TAG, "provider: "+currentLocation.getProvider());
            } else if (ActivityRecognitionResult.hasResult(intent)){
                DetectedActivity probableActivity = getDetectedActivity(intent);
                int confidence = probableActivity.getConfidence();
                StringBuilder sb = new StringBuilder()
                    .append(System.currentTimeMillis())
                    .append(",")
                    .append(getFriendlyName(probableActivity.getType()))
                    .append(",")
                    .append(confidence)
                    .append(",")
                    .append(loc);
                appendLog(sb.toString());
                Log.d(TAG, "Detected Activity Received");
            }
        }
    }



    private DetectedActivity getDetectedActivity(Intent intent) {
        ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
        return result.getMostProbableActivity();
    }

    protected String getLocationString(){
        String lat,lng;

        if ( currentLocation != null && (System.currentTimeMillis() - locationTimeStamp < 10000)){
            lat = String.valueOf(mLat);
            lng = String.valueOf(mLng);
        } else {
            lat = "null";
            lng = "null";
        }

        return lat+","+lng;
    }

    public static void appendLog(String text)
    {
        File logFile = new File(Environment.getExternalStorageDirectory().getPath()+"/activityDetection.log");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * When supplied with the integer representation of the activity returns the activity as friendly string
     * @param detected_activity_type the DetectedActivity.getType()
     * @return a friendly string of the detected activity
     */
    private static String getFriendlyName(int detected_activity_type){
        switch (detected_activity_type) {
            case DetectedActivity.IN_VEHICLE:
                return "IN_VEHICLE";
            case DetectedActivity.ON_BICYCLE:
                return "ON_BICYCLE";
            case DetectedActivity.ON_FOOT:
                return "ON_FOOT";
            case DetectedActivity.TILTING:
                return "TILTING";
            case DetectedActivity.STILL:
                return "STILL";
            case DetectedActivity.RUNNING:
                return "RUNNING";
            case DetectedActivity.WALKING:
                return "WALKING";
            default:
                return "UNKNOWN";
        }
    }

}

package com.parkopedia.parkingdetection;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;

public class LogWriterDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.dialog_log_writer, null))
            // Add action buttons
            .setPositiveButton(R.string.write, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    EditText e = (EditText) ((AlertDialog)dialog).findViewById(R.id.logMsg);
                    String msg = e.getText().toString();
                    StringBuilder sb = new StringBuilder();
                    sb.append(System.currentTimeMillis())
                        .append(", MESSAGE,")
                        .append(msg);
                    final String line = sb.toString();
                    Thread t = new Thread() {
                        @Override
                        public void run() {
                            ActivityRecognitionIntentService.appendLog(line);
                        }
                    };
                    t.start();

                }
            })
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    LogWriterDialog.this.getDialog().cancel();
                }
            });

        return builder.create();
    }

}

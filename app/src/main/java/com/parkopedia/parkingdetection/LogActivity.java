package com.parkopedia.parkingdetection;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

public class LogActivity extends ActionBarActivity {

    private Timer mTimer;
    private TextView output;

    final Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        output=(TextView) findViewById(R.id.log);
        output.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                displayOutput();
            }
        }, 100, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTimer.cancel();
    }

    public void displayOutput()
    {
        Thread t = new Thread(){
            @Override
            public void run() {
                File sdcard = Environment.getExternalStorageDirectory();
                File file = new File(sdcard,"/activityDetection.log");
                final String updateText = tail(file, 50);
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        output.setText(updateText);
                    }
                });
            }
        };
        t.start();
    }

    public String tail(File file, int lines) {
        java.io.RandomAccessFile fileHandler = null;
        try {
            fileHandler =
                new java.io.RandomAccessFile( file, "r" );
            long fileLength = fileHandler.length() - 1;
            StringBuilder sb = new StringBuilder();
            int line = 0;

            for(long filePointer = fileLength; filePointer != -1; filePointer--){
                fileHandler.seek( filePointer );
                int readByte = fileHandler.readByte();

                if( readByte == 0xA ) {
                    line = line + 1;
                    if (line == lines) {
                        if (filePointer == fileLength) {
                            continue;
                        }
                        break;
                    }
                } else if( readByte == 0xD ) {
                    line = line + 1;
                    if (line == lines) {
                        if (filePointer == fileLength - 1) {
                            continue;
                        }
                        break;
                    }
                }
                sb.append( ( char ) readByte );
            }

            String lastLine = sb.reverse().toString();
            return lastLine;
        } catch( java.io.FileNotFoundException e ) {
            e.printStackTrace();
            return null;
        } catch( java.io.IOException e ) {
            e.printStackTrace();
            return null;
        }
        finally {
            if (fileHandler != null )
                try {
                    fileHandler.close();
                } catch (IOException e) {
                }
        }
    }
}
